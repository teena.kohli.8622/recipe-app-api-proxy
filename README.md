# Recipe App API Proxy

NGINX proxy app for our recipe app API

## Usage

*  [Architecture-V3.png](./reference/Architecture-V3.png) Architecture Diagram
 * [commands-reference.md](./reference/commands-reference.md) - A reference sheet with the common commands used in this course.
 * [Makefile](./reference/Makefile) - Sample `Makefile` which could be added to your project to reduce the length of the commands you need to run.
 * [windows-notes.md](./reference/windows-notes.md) - Notes and tips for running the steps on Windows machines.
 
